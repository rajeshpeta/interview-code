# syntax=docker/dockerfile:1
FROM node:12-alpine
RUN apk add --no-cache python2 g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
EXPOSE 3000


FROM tomcat:7.0.107
RUN sed -i 's/port="8080"/port="4287"/' ${CATALINA_HOME}/conf/server.xml
ADD ./tomcat-cas/war/ ${CATALINA_HOME}/webapps/
CMD ["catalina.sh", "run"]

docker build -f Dockerfile -t repositporyname .
docker images
docker run -p 8080:8080 -d image id


docker tag imageid rajeshpeta123/reposotoryname:commitmessage
docker push rajeshpeta123/repositoryname

